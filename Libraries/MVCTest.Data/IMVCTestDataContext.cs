﻿namespace MVCTest.Data
{
    public interface IMVCTestDataContext
    {
        MVCTestDbContext MVCTestDbDataContext();
    }
}
