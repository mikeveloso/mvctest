﻿namespace MVCTest.Data
{
    public class MVCTestDataContext : IMVCTestDataContext
    {
        private readonly MVCTestDbContext dbContext;

        public MVCTestDataContext()
        {
            dbContext = new MVCTestDbContext();
        }

        public MVCTestDbContext MVCTestDbDataContext()
        {
            return dbContext;
        }
    }
}
