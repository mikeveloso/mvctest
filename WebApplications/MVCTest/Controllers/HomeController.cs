﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCTest.Models;
using MVCTest.Services;

namespace MVCTest.Controllers
{
    public class HomeController : Controller
    {
        private readonly IEmployeeService service;

        public HomeController(IEmployeeService employeeService)
        {
            service = employeeService;
        }

        public ActionResult Index()
        {
            List<Employee> employees = service.GetEmployees()?.ToList();

            return View(employees);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}