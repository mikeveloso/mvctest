﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MVCTest.Models
{
    public class Employee_Works_Shift
    {
        [Key]
        public int Work_Shift_ID { get; set; }

        public int Employee_ID { get; set; }
                
        public int Shift_ID { get; set; }

        [ForeignKey("Shift_ID")]
        public virtual Shift ShiftDetail { get; set; }
    }
}
