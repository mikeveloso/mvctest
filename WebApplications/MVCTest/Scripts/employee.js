﻿$(document).ready(function () {
    loadEmployees();

    $('.btn-close').click(function (e) {
        $('#shift-details').addClass('hide-popup');
    });

    $('.btn-filter').click(function (e) {
        e.preventDefault();

        $('.filter-popup').removeClass('hide-popup');
    });

    $('.btn-filter-close').click(function (e) {
        e.preventDefault();

        $('.filter-popup').addClass('hide-popup');
    });

    $('.filter-item').click(function (e) {
        e.preventDefault();

        var employeeId = parseInt($(this).attr('data-employee-id'));

        if (employeeId === 0 || employeeId === null || employeeId === undefined) {
            loadEmployees();
        } else {
            filterResult(employeeId);
        }

        $('.filter-popup').addClass('hide-popup');
    });

    $('.btn-view-shift').click(function (e) {
        e.preventDefault();
        alert('hello');
    });
});

function loadEmployees() {
    $.ajax({
        url: '/api/employees/getemployees',
        dataType: 'json',
        success: function (result) {
            empirontableOptions = {
                data: result,
                nodatatext: "No employee(s) available.",
                sortable: false,
                scrollable: true,
                draggableColumns: false,
                fitHeight: true,
                fixedheader: true,
                columns: [
                    { title: "Employee ID", datafield: "Employee_ID", visible: true, width: "15%" },
                    { title: "First Name", datafield: "FirstName", width: "25%" },
                    { title: "Surname", datafield: "Surname", width: "25%" },
                    { title: "Work Hours", datafield: "TotalWorkHours", width: "10%" },
                    {
                        title: "Shift Detail(s)",
                        datafield: "Actions",
                        width: "25%",
                        view: function (data) {
                            return '<a href="javascript:;" onclick="showShiftDetail(' + data.Employee_ID + ', \'' + data.FirstName + ' ' + data.Surname + '\');" data-employee-name="' + data.FirstName + ' ' + data.Surname + '">View</a>'
                        }
                    },
                ]
            };

            var empirontable = $('#employees').JSIronTable(empirontableOptions);
        }
    });
}

function filterResult(employeeId) {
    $.ajax({
        url: '/api/employees/' + employeeId,
        dataType: 'json',
        success: function (result) {
            empirontableOptions = {
                data: result,
                nodatatext: "Employee not found.",
                sortable: false,
                scrollable: true,
                draggableColumns: false,
                fitHeight: true,
                fixedheader: true,
                columns: [
                    { title: "Employee ID", datafield: "Employee_ID", visible: true, width: "15%" },
                    { title: "First Name", datafield: "FirstName", width: "25%" },
                    { title: "Surname", datafield: "Surname", width: "25%" },
                    { title: "Work Hours", datafield: "TotalWorkHours", width: "10%" },
                    {
                        title: "Shift Detail(s)",
                        datafield: "Actions",
                        width: "25%",
                        view: function (data) {
                            return '<a href="javascript:;" onclick="showShiftDetail(' + data.Employee_ID + ')">View</a>'
                        }
                    },
                ]
            };

            var empirontable = $('#employees').JSIronTable(empirontableOptions);
        }
    });
}

function showShiftDetail(employeeId, employeeName) {
    $('#shift-details').addClass('hide-popup');
    var apiUrl = '/api/employees/' + employeeId + '/shift-details';
    $('.shift-detail-title h3').text(employeeName + ' Work Shift Detail(s)');

    $.ajax({
        url: apiUrl,
        dataType: 'json',
        success: function (shifts) {
            $('#shift-details').removeClass('hide-popup');

            shiftirontableOptions = {
                data: shifts,
                nodatatext: "No shift(s) available.",
                sortable: false,
                draggableColumns: false,
                scrollable: true,
                fitHeight: true,
                fixedheader: true,
                columns: [
                    { title: "Date", datafield: "ShiftDate", visible: true, width: "25%" },
                    { title: "Name", datafield: "ShiftName", width: "25%" },
                    { title: "Shift Start", datafield: "ShiftStart", width: "25%" },
                    { title: "Shift End", datafield: "ShiftEnd", width: "25%" },
                ]
            };

            var shiftirontable = $('#shiftdetail').JSIronTable(shiftirontableOptions);
        }
    });
}