using MVCTest.Data;
using MVCTest.Repository;
using MVCTest.Services;
using System.Web.Mvc;
using Unity;
using Unity.Mvc5;

namespace MVCTest
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            UnityContainer container = new UnityContainer();

            // register all your components with the container here
            RegisterDbContexts(container);
            RegisterRepositories(container);
            RegisterServices(container);

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }

        private static void RegisterDbContexts(UnityContainer container)
        {
            container.RegisterType<IMVCTestDataContext, MVCTestDataContext>();
        }

        private static void RegisterRepositories(UnityContainer container)
        {
            container.RegisterType<IEmployeeRepository, EmployeeRepository>();
        }

        private static void RegisterServices(UnityContainer container)
        {
            container.RegisterType<IEmployeeService, EmployeeService>();
        }
    }
}