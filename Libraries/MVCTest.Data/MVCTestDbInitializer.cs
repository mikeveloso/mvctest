﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using MVCTest.Models;

namespace MVCTest.Data
{
    public class MVCTestDbInitializer : CreateDatabaseIfNotExists<MVCTestDbContext>
    {
        protected override void Seed(MVCTestDbContext context)
        {
            SeedEmployees(context);
            SeedShifts(context);
            SeedWorkShifts(context);            
            SetTableConstraints(context);
            base.Seed(context);
        }

        private void SeedEmployees(MVCTestDbContext dbContext)
        {
            StringBuilder employeeSqlCommand = new StringBuilder(string.Empty);

            employeeSqlCommand.Append("SET IDENTITY_INSERT [dbo].[Employee] ON;\r\n");
            employeeSqlCommand.Append("INSERT INTO[dbo].[Employee] ([Employee_ID], [FirstName], [Surname]) VALUES(1, N'James', N'Brown');\r\n");
            employeeSqlCommand.Append("INSERT INTO[dbo].[Employee]([Employee_ID], [FirstName], [Surname]) VALUES(2, N'Harry', N'Potter')\r\n");
            employeeSqlCommand.Append("INSERT INTO[dbo].[Employee]([Employee_ID], [FirstName], [Surname]) VALUES(3, N'Alice', N'Cooper');\r\n");
            employeeSqlCommand.Append("INSERT INTO[dbo].[Employee]([Employee_ID], [FirstName], [Surname]) VALUES(4, N'Joe', N'Cocker');\r\n");
            employeeSqlCommand.Append("INSERT INTO[dbo].[Employee]([Employee_ID], [FirstName], [Surname]) VALUES(5, N'Neil', N'Young');\r\n");
            employeeSqlCommand.Append("INSERT INTO[dbo].[Employee]([Employee_ID], [FirstName], [Surname]) VALUES(6, N'Rob', N'Roy');\r\n");
            employeeSqlCommand.Append("SET IDENTITY_INSERT [dbo].[Employee] OFF;\r\n");
            
            dbContext.Database.ExecuteSqlCommand(employeeSqlCommand.ToString());
        }

        private void SeedWorkShifts(MVCTestDbContext dbContext)
        {
            StringBuilder workShiftSqlCommand = new StringBuilder(string.Empty);

            workShiftSqlCommand.Append("INSERT INTO [dbo].[Employee_Works_Shift] ([Employee_ID], [Shift_ID]) VALUES (1, 1)");
            workShiftSqlCommand.Append("INSERT INTO[dbo].[Employee_Works_Shift]([Employee_ID], [Shift_ID]) VALUES(2, 1)");
            workShiftSqlCommand.Append("INSERT INTO[dbo].[Employee_Works_Shift]([Employee_ID], [Shift_ID]) VALUES(3, 1)");
            workShiftSqlCommand.Append("INSERT INTO[dbo].[Employee_Works_Shift]([Employee_ID], [Shift_ID]) VALUES(4, 1)");
            workShiftSqlCommand.Append("INSERT INTO[dbo].[Employee_Works_Shift]([Employee_ID], [Shift_ID]) VALUES(1, 2)");
            workShiftSqlCommand.Append("INSERT INTO[dbo].[Employee_Works_Shift]([Employee_ID], [Shift_ID]) VALUES(5, 2)");
            workShiftSqlCommand.Append("INSERT INTO[dbo].[Employee_Works_Shift]([Employee_ID], [Shift_ID]) VALUES(6, 2)");
            workShiftSqlCommand.Append("INSERT INTO[dbo].[Employee_Works_Shift]([Employee_ID], [Shift_ID]) VALUES(1, 4)");
            workShiftSqlCommand.Append("INSERT INTO[dbo].[Employee_Works_Shift]([Employee_ID], [Shift_ID]) VALUES(4, 4)");
            workShiftSqlCommand.Append("INSERT INTO[dbo].[Employee_Works_Shift]([Employee_ID], [Shift_ID]) VALUES(6, 4)");
            workShiftSqlCommand.Append("INSERT INTO[dbo].[Employee_Works_Shift]([Employee_ID], [Shift_ID]) VALUES(2, 4)");
            workShiftSqlCommand.Append("INSERT INTO[dbo].[Employee_Works_Shift]([Employee_ID], [Shift_ID]) VALUES(6, 5)");
            workShiftSqlCommand.Append("INSERT INTO[dbo].[Employee_Works_Shift]([Employee_ID], [Shift_ID]) VALUES(2, 5)");
            workShiftSqlCommand.Append("INSERT INTO[dbo].[Employee_Works_Shift]([Employee_ID], [Shift_ID]) VALUES(4, 5)");
            workShiftSqlCommand.Append("INSERT INTO[dbo].[Employee_Works_Shift]([Employee_ID], [Shift_ID]) VALUES(5, 5);\r\n");
            workShiftSqlCommand.Append("INSERT INTO[dbo].[Employee_Works_Shift]([Employee_ID], [Shift_ID]) VALUES(1, 6);\r\n");
            workShiftSqlCommand.Append("INSERT INTO[dbo].[Employee_Works_Shift]([Employee_ID], [Shift_ID]) VALUES(2, 6);\r\n");
            workShiftSqlCommand.Append("INSERT INTO[dbo].[Employee_Works_Shift]([Employee_ID], [Shift_ID]) VALUES(3, 6);\r\n");
            workShiftSqlCommand.Append("INSERT INTO[dbo].[Employee_Works_Shift]([Employee_ID], [Shift_ID]) VALUES(6, 6);\r\n");
            workShiftSqlCommand.Append("INSERT INTO[dbo].[Employee_Works_Shift]([Employee_ID], [Shift_ID]) VALUES(3, 7);\r\n");
            workShiftSqlCommand.Append("INSERT INTO[dbo].[Employee_Works_Shift]([Employee_ID], [Shift_ID]) VALUES(5, 7);\r\n");
            workShiftSqlCommand.Append("INSERT INTO[dbo].[Employee_Works_Shift]([Employee_ID], [Shift_ID]) VALUES(2, 7);\r\n");
            workShiftSqlCommand.Append("INSERT INTO[dbo].[Employee_Works_Shift]([Employee_ID], [Shift_ID]) VALUES(1, 8);\r\n");
            workShiftSqlCommand.Append("INSERT INTO[dbo].[Employee_Works_Shift]([Employee_ID], [Shift_ID]) VALUES(5, 8);\r\n");
            workShiftSqlCommand.Append("INSERT INTO[dbo].[Employee_Works_Shift]([Employee_ID], [Shift_ID]) VALUES(2, 8);\r\n");
            workShiftSqlCommand.Append("INSERT INTO[dbo].[Employee_Works_Shift]([Employee_ID], [Shift_ID]) VALUES(3, 8);\r\n");

            dbContext.Database.ExecuteSqlCommand(workShiftSqlCommand.ToString());
        }

        private void SeedShifts(MVCTestDbContext dbContext)
        {
            StringBuilder shiftSqlCommand = new StringBuilder(string.Empty);

            shiftSqlCommand.Append("SET IDENTITY_INSERT [dbo].[Shifts] ON;\r\n");
            shiftSqlCommand.Append("INSERT INTO [dbo].[Shifts] ([Shift_ID], [Shift_Start], [Shift_End], [Shift_Name]) VALUES (1, CAST(N'11 Apr 2020 09:00:00' AS DateTime), CAST(N'11 Apr 2020 17:00:00' AS DateTime), N'Morning 9-17');\r\n");
            shiftSqlCommand.Append("INSERT INTO[dbo].[Shifts]([Shift_ID], [Shift_Start], [Shift_End], [Shift_Name]) VALUES (2, CAST(N'12 Apr 2020 10:00:00' AS DateTime), CAST(N'12 Apr 2020 14:00:00' AS DateTime), N'Morning 10-14');\r\n");
            shiftSqlCommand.Append("INSERT INTO[dbo].[Shifts]([Shift_ID], [Shift_Start], [Shift_End], [Shift_Name]) VALUES (4, CAST(N'13 Apr 2020 09:00:00' AS DateTime), CAST(N'13 Apr 2020 17:00:00' AS DateTime), N'Morning 9-17');\r\n");
            shiftSqlCommand.Append("INSERT INTO[dbo].[Shifts]([Shift_ID], [Shift_Start], [Shift_End], [Shift_Name]) VALUES (5, CAST(N'14 Apr 2020 10:00:00' AS DateTime), CAST(N'14 Apr 2020 14:00:00' AS DateTime), N'Morning 10-14');\r\n");
            shiftSqlCommand.Append("INSERT INTO[dbo].[Shifts]([Shift_ID], [Shift_Start], [Shift_End], [Shift_Name]) VALUES (6, CAST(N'15 Apr 2020 09:00:00' AS DateTime), CAST(N'15 Apr 2020 17:00:00' AS DateTime), N'Morning 9-17');\r\n");
            shiftSqlCommand.Append("INSERT INTO[dbo].[Shifts]([Shift_ID], [Shift_Start], [Shift_End], [Shift_Name]) VALUES (7, CAST(N'13 May 2020 09:00:00' AS DateTime), CAST(N'13 May 2020 17:00:00' AS DateTime), N'Morning 9-17');\r\n");
            shiftSqlCommand.Append("INSERT INTO[dbo].[Shifts]([Shift_ID], [Shift_Start], [Shift_End], [Shift_Name]) VALUES (8, CAST(N'14 May 2020 09:00:00' AS DateTime), CAST(N'14 May 2020 17:00:00' AS DateTime), N'Morning 9-17');\r\n");
            shiftSqlCommand.Append("SET IDENTITY_INSERT [dbo].[Shifts] OFF;\r\n");

            dbContext.Database.ExecuteSqlCommand(shiftSqlCommand.ToString());
        }

        private void SetTableConstraints(MVCTestDbContext dbContext)
        {
            dbContext.Database.ExecuteSqlCommand("ALTER TABLE[dbo].[Employee_Works_Shift]  WITH CHECK ADD CONSTRAINT [FK_Employee_Works_Shift_Employee] FOREIGN KEY([Employee_ID]) REFERENCES[dbo].[Employee]([Employee_ID])");
            dbContext.Database.ExecuteSqlCommand("ALTER TABLE [dbo].[Employee_Works_Shift] CHECK CONSTRAINT [FK_Employee_Works_Shift_Employee]");
            dbContext.Database.ExecuteSqlCommand("ALTER TABLE [dbo].[Employee_Works_Shift]  WITH CHECK ADD  CONSTRAINT [FK_Employee_Works_Shift_Shifts] FOREIGN KEY([Shift_ID]) REFERENCES[dbo].[Shifts]([Shift_ID])");
            dbContext.Database.ExecuteSqlCommand("ALTER TABLE [dbo].[Employee_Works_Shift] CHECK CONSTRAINT [FK_Employee_Works_Shift_Shifts]");
        }
    }
}
