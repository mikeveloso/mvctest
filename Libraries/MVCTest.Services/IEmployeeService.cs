﻿using System;
using System.Collections.Generic;
using System.Linq;

using MVCTest.Models;

namespace MVCTest.Services
{
    public interface IEmployeeService
    {
        IEnumerable<Employee> GetEmployees();

        IEnumerable<Employee> GetEmployees(int employeeId);

        IEnumerable<ShiftDetail> GetEmployeeWorkShifts(int employeeId);
    }
}
