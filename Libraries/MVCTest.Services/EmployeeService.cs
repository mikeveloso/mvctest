﻿using System;
using System.Collections.Generic;
using System.Linq;

using MVCTest.Models;
using MVCTest.Repository;

namespace MVCTest.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IEmployeeRepository repository;

        public EmployeeService(IEmployeeRepository employeeRepository)
        {
            repository = employeeRepository;
        }

        public IEnumerable<Employee> GetEmployees()
        {
            return repository.GetEmployees().ToList();
        }

        public IEnumerable<Employee> GetEmployees(int employeeId)
        {
            return repository.GetEmployees()
                .Where(e => e.Employee_ID == employeeId)
                .ToList();
        }

        public IEnumerable<ShiftDetail> GetEmployeeWorkShifts(int employeeId)
        {
            List<ShiftDetail> shiftDetails = new List<ShiftDetail>();

            List<Employee_Works_Shift> workShifts = repository.GetEmployeeWorkShifts(employeeId)?.ToList();

            if (workShifts != null && workShifts.Any())
            {
                workShifts.ForEach(workShift =>
                {
                    if (workShift.ShiftDetail != null)
                    {
                        ShiftDetail shiftDetail = new ShiftDetail
                        {
                            ShiftDate = workShift.ShiftDetail.Shift_Start.ToString("dd MMM yyyy"),
                            ShiftName = workShift.ShiftDetail.Shift_Name,
                            ShiftStart = workShift.ShiftDetail.Shift_Start.ToString("hh:mm"),
                            ShiftEnd = workShift.ShiftDetail.Shift_End.ToString("hh:mm"),
                        };

                        shiftDetails.Add(shiftDetail);
                    }
                });
            }

            return shiftDetails;
        }
    }
}
