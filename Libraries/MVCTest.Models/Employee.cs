﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace MVCTest.Models
{
    public class Employee
    {
        public Employee()
        {
            Work_Shifts = new List<Employee_Works_Shift>();
        }

        [Key]
        public int Employee_ID { get; set; }

        public string FirstName { get; set; }

        public string Surname { get; set; }
        
        [ForeignKey("Employee_ID")]
        public List<Employee_Works_Shift> Work_Shifts { get; set; }

        [NotMapped]
        public double TotalWorkHours 
        {
            get 
            {
                double totalWorkHours = 0;

                if (Work_Shifts != null && Work_Shifts.Any())
                {
                    Work_Shifts.ForEach(workShift =>
                    {
                        if (workShift.ShiftDetail != null)
                        {
                            DateTime startTime = workShift.ShiftDetail.Shift_Start;
                            DateTime endTime = workShift.ShiftDetail.Shift_End;

                            totalWorkHours += endTime.Subtract(startTime).TotalHours;
                        }
                    });
                }

                return totalWorkHours;
            }
        }

        [NotMapped]
        public string Actions { get; set; } = "";
    }
}
