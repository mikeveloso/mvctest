﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

using Unity;

using MVCTest.Data;
using MVCTest.Repository;
using MVCTest.Services;
using MVCTest.UnityResolvers;

namespace MVCTest
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            UnityContainer container = new UnityContainer();
            
            RegisterDbContexts(container);
            RegisterRepositories(container);
            RegisterServices(container);
            
            config.DependencyResolver = new UnityResolver(container);

            var matches = config.Formatters
                           .Where(f => f.SupportedMediaTypes
                                        .Where(m => m.MediaType.ToString() == "application/xml" ||
                                                    m.MediaType.ToString() == "text/xml")
                                        .Count() > 0)
                           .ToList();
            foreach (var match in matches)
                config.Formatters.Remove(match);

            config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }

        private static void RegisterDbContexts(UnityContainer container)
        {
            container.RegisterType<IMVCTestDataContext, MVCTestDataContext>();
        }

        private static void RegisterRepositories(UnityContainer container)
        {
            container.RegisterType<IEmployeeRepository, EmployeeRepository>();
        }

        private static void RegisterServices(UnityContainer container)
        {
            container.RegisterType<IEmployeeService, EmployeeService>();
        }
    }
}
