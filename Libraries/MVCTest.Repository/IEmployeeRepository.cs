﻿using System.Collections.Generic;

using MVCTest.Models;

namespace MVCTest.Repository
{
    public interface IEmployeeRepository
    {
        IEnumerable<Employee> GetEmployees();

        Employee GetEmployee(int employeeId);

        IEnumerable<Employee_Works_Shift> GetEmployeeWorkShifts(int employeeId);
    }
}
