CREATE TABLE [dbo].[Employee]( 
 	[Employee_ID] [int] IDENTITY(1,1) NOT NULL, 
 	[FirstName] [varchar](50) NOT NULL, 
 	[Surname] [varchar](50) NOT NULL, 
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED  
( 
 	[Employee_ID] ASC 
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, 
ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY] 
) ON [PRIMARY] 
 
 
CREATE TABLE [dbo].[Employee_Works_Shift]( 
	[Work_Shift_ID] [int] IDENTITY(1,1) NOT NULL, 
 	[Employee_ID] [int] NOT NULL, 
 	[Shift_ID] [int] NOT NULL, 
 CONSTRAINT [PK_Employee_Works_Shift] PRIMARY KEY CLUSTERED  
( 
 	[Work_Shift_ID] ASC 
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, 
ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY] 
) ON [PRIMARY] 
 
 
CREATE TABLE [dbo].[Shifts]( 
 	[Shift_ID] [int] IDENTITY(1,1) NOT NULL, 
 	[Shift_Start] [datetime] NOT NULL, 
 	[Shift_End] [datetime] NOT NULL, 
 	[Shift_Name] [varchar](50) NOT NULL, 
 CONSTRAINT [PK_Shifts] PRIMARY KEY CLUSTERED  
( 
 	[Shift_ID] ASC 
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, 
ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY] ) ON [PRIMARY] 
 
 
SET IDENTITY_INSERT  [dbo].[Employee] ON  
 
GO 
INSERT INTO [dbo].[Employee] ([Employee_ID], [FirstName], [Surname]) VALUES (1, N'James', N'Brown') 
GO 
INSERT INTO [dbo].[Employee] ([Employee_ID], [FirstName], [Surname]) VALUES (2, N'Harry', N'Potter')
GO
INSERT INTO [dbo].[Employee] ([Employee_ID], [FirstName], [Surname]) VALUES (3, N'Alice', N'Cooper')
GO
INSERT INTO [dbo].[Employee] ([Employee_ID], [FirstName], [Surname]) VALUES (4, N'Joe', N'Cocker')
GO
INSERT INTO [dbo].[Employee] ([Employee_ID], [FirstName], [Surname]) VALUES (5, N'Neil', N'Young')
GO 
INSERT INTO [dbo].[Employee] ([Employee_ID], [FirstName], [Surname]) VALUES (6, N'Rob', N'Roy') 
GO 
SET IDENTITY_INSERT  [dbo].[Employee] OFF 
GO 
INSERT INTO [dbo].[Employee_Works_Shift] ([Employee_ID], [Shift_ID]) VALUES (1, 1) 
GO 
INSERT INTO [dbo].[Employee_Works_Shift] ([Employee_ID], [Shift_ID]) VALUES (2, 1)
GO 
INSERT INTO [dbo].[Employee_Works_Shift] ([Employee_ID], [Shift_ID]) VALUES (3, 1)
GO 
INSERT INTO [dbo].[Employee_Works_Shift] ([Employee_ID], [Shift_ID]) VALUES (4, 1) 
GO 
INSERT INTO [dbo].[Employee_Works_Shift] ([Employee_ID], [Shift_ID]) VALUES (1, 2)
GO 
INSERT INTO [dbo].[Employee_Works_Shift] ([Employee_ID], [Shift_ID]) VALUES (5, 2)
GO 
INSERT INTO [dbo].[Employee_Works_Shift] ([Employee_ID], [Shift_ID]) VALUES (6, 2)
GO 
INSERT INTO [dbo].[Employee_Works_Shift] ([Employee_ID], [Shift_ID]) VALUES (1, 4)
GO 
INSERT INTO [dbo].[Employee_Works_Shift] ([Employee_ID], [Shift_ID]) VALUES (4, 4) 
GO 
INSERT INTO [dbo].[Employee_Works_Shift] ([Employee_ID], [Shift_ID]) VALUES (6, 4)
GO 
INSERT INTO [dbo].[Employee_Works_Shift] ([Employee_ID], [Shift_ID]) VALUES (2, 4)
GO 
INSERT INTO [dbo].[Employee_Works_Shift] ([Employee_ID], [Shift_ID]) VALUES (6, 5)
GO 
INSERT INTO [dbo].[Employee_Works_Shift] ([Employee_ID], [Shift_ID]) VALUES (2, 5)
GO 
INSERT INTO [dbo].[Employee_Works_Shift] ([Employee_ID], [Shift_ID]) VALUES (4, 5) 
GO 
INSERT INTO [dbo].[Employee_Works_Shift] ([Employee_ID], [Shift_ID]) VALUES (5, 5)
GO 
INSERT INTO [dbo].[Employee_Works_Shift] ([Employee_ID], [Shift_ID]) VALUES (1, 6)
GO 
INSERT INTO [dbo].[Employee_Works_Shift] ([Employee_ID], [Shift_ID]) VALUES (2, 6)
GO 
INSERT INTO [dbo].[Employee_Works_Shift] ([Employee_ID], [Shift_ID]) VALUES (3, 6)
GO 
INSERT INTO [dbo].[Employee_Works_Shift] ([Employee_ID], [Shift_ID]) VALUES (6, 6) 
GO 
INSERT INTO [dbo].[Employee_Works_Shift] ([Employee_ID], [Shift_ID]) VALUES (3, 7)
GO 
INSERT INTO [dbo].[Employee_Works_Shift] ([Employee_ID], [Shift_ID]) VALUES (5, 7)
GO 
INSERT INTO [dbo].[Employee_Works_Shift] ([Employee_ID], [Shift_ID]) VALUES (2, 7) 
GO 
INSERT INTO [dbo].[Employee_Works_Shift] ([Employee_ID], [Shift_ID]) VALUES (1, 8) 
GO 
INSERT INTO [dbo].[Employee_Works_Shift] ([Employee_ID], [Shift_ID]) VALUES (5, 8)
GO 
INSERT INTO [dbo].[Employee_Works_Shift] ([Employee_ID], [Shift_ID]) VALUES (2, 8) 
GO 
INSERT INTO [dbo].[Employee_Works_Shift] ([Employee_ID], [Shift_ID]) VALUES (3, 8)
GO 
SET IDENTITY_INSERT  [dbo].[Shifts] ON  
 
GO 
INSERT INTO [dbo].[Shifts] ([Shift_ID], [Shift_Start], [Shift_End], [Shift_Name]) VALUES 
(1, CAST(N'11 Apr 2020 09:00:00' AS DateTime), CAST(N'11 Apr 2020 17:00:00' AS DateTime), N'Morning 9-17') 
GO 
INSERT INTO [dbo].[Shifts] ([Shift_ID], [Shift_Start], [Shift_End], [Shift_Name]) VALUES 
(2, CAST(N'12 Apr 2020 10:00:00' AS DateTime), CAST(N'12 Apr 2020 14:00:00' AS DateTime), N'Morning 10-14') 
GO 
INSERT INTO [dbo].[Shifts] ([Shift_ID], [Shift_Start], [Shift_End], [Shift_Name]) VALUES 
(4, CAST(N'13 Apr 2020 09:00:00' AS DateTime), CAST(N'13 Apr 2020 17:00:00' AS DateTime), N'Morning 9-17') 
GO 
INSERT INTO [dbo].[Shifts] ([Shift_ID], [Shift_Start], [Shift_End], [Shift_Name]) VALUES 
(5, CAST(N'14 Apr 2020 10:00:00' AS DateTime), CAST(N'14 Apr 2020 14:00:00' AS DateTime), N'Morning 10-14') 
GO 
INSERT INTO [dbo].[Shifts] ([Shift_ID], [Shift_Start], [Shift_End], [Shift_Name]) VALUES 
(6, CAST(N'15 Apr 2020 09:00:00' AS DateTime), CAST(N'15 Apr 2020 17:00:00' AS DateTime), N'Morning 9-17') 
GO 
INSERT INTO [dbo].[Shifts] ([Shift_ID], [Shift_Start], [Shift_End], [Shift_Name]) VALUES 
(7, CAST(N'13 May 2020 09:00:00' AS DateTime), CAST(N'13 May 2020 17:00:00' AS DateTime), N'Morning 9-17') 
GO 
INSERT INTO [dbo].[Shifts] ([Shift_ID], [Shift_Start], [Shift_End], [Shift_Name]) VALUES 
(8, CAST(N'14 May 2020 09:00:00' AS DateTime), CAST(N'14 May 2020 17:00:00' AS DateTime), N'Morning 9-17') 
GO 
SET IDENTITY_INSERT  [dbo].[Shifts] OFF 
GO 
ALTER TABLE [dbo].[Employee_Works_Shift]  WITH CHECK ADD  CONSTRAINT 
[FK_Employee_Works_Shift_Employee] FOREIGN KEY([Employee_ID]) 
REFERENCES [dbo].[Employee] ([Employee_ID]) 
GO 
ALTER TABLE [dbo].[Employee_Works_Shift] CHECK CONSTRAINT 
[FK_Employee_Works_Shift_Employee] 
GO 
ALTER TABLE [dbo].[Employee_Works_Shift]  WITH CHECK ADD  CONSTRAINT 
[FK_Employee_Works_Shift_Shifts] FOREIGN KEY([Shift_ID]) 
REFERENCES [dbo].[Shifts] ([Shift_ID]) 
GO 
ALTER TABLE [dbo].[Employee_Works_Shift] CHECK CONSTRAINT 
[FK_Employee_Works_Shift_Shifts] 
GO