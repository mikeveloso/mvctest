﻿using System.Data.Entity;
using MVCTest.Models;

namespace MVCTest.Data
{
    public partial class MVCTestDbContext : DbContext
    {
        public MVCTestDbContext() : base("name=MVCTextDbConnection")
        {
            Database.SetInitializer(new MVCTestDbInitializer());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>().ToTable("Employee")
                .HasMany(e => e.Work_Shifts);

            modelBuilder.Entity<Employee_Works_Shift>().ToTable("Employee_Works_Shift")
                .HasRequired(ws => ws.ShiftDetail);                
                                
            modelBuilder.Entity<Shift>().ToTable("Shifts");
        }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<Employee_Works_Shift> Employee_Works_Shifts { get; set; }

        public DbSet<Shift> Shifts { get; set; }
    }
}
