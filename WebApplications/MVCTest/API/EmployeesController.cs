﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using MVCTest.Models;
using MVCTest.Services;

namespace MVCTest.API
{
    public class EmployeesController : ApiController
    {
        private readonly IEmployeeService service;

        public EmployeesController(IEmployeeService employeeService)
        {
            service = employeeService;
        }
        
        [Route("api/employees/getemployees")]
        public IHttpActionResult GetEmployees()
        {
            try
            {
                List<Employee> employees = service.GetEmployees()?.ToList();

                if (employees != null && employees.Any())
                    return Json(employees);
                else
                    return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [Route("api/employees/{id}")]
        public IHttpActionResult GetEmployees(int id)
        {
            try
            {
                List<Employee> employees = service.GetEmployees(id)?.ToList();

                if(employees != null && employees.Any())
                    return Json(employees);
                else
                    return NotFound();

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("api/employees/{id}/shift-details")]
        public IHttpActionResult GetEmployeeShiftDetails(int id)
        {
            try
            {
                List<ShiftDetail> shiftDetails = service.GetEmployeeWorkShifts(id)?.ToList();

                if (shiftDetails != null && shiftDetails.Any())
                    return Json(shiftDetails);
                else
                    return NotFound();

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
