﻿using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;

using MVCTest.Data;
using MVCTest.Models;

namespace MVCTest.Repository
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private readonly MVCTestDbContext dbContext;

        public EmployeeRepository(IMVCTestDataContext dataContext)
        {
            dbContext = dataContext.MVCTestDbDataContext();
        }

        public IEnumerable<Employee> GetEmployees()
        {
            return dbContext.Employees.Include(e => e.Work_Shifts).ToList();
        }

        public Employee GetEmployee(int employeeId)
        {
            return dbContext.Employees.Include(e => e.Work_Shifts).Where(e => e.Employee_ID == employeeId).FirstOrDefault();
        }

        public IEnumerable<Employee_Works_Shift> GetEmployeeWorkShifts(int employeeId)
        {
            return dbContext.Employee_Works_Shifts.Include(ws => ws.ShiftDetail).Where(e => e.Employee_ID == employeeId).ToList();
        }
    }
}
