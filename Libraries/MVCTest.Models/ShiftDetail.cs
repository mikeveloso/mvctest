﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MVCTest.Models
{
    public class ShiftDetail
    {
        public string ShiftDate { get; set; }

        public string ShiftName { get; set; }

        public string ShiftStart { get; set; }

        public string ShiftEnd { get; set; }
    }
}
